require 'httparty'
require 'json'
RSpec.describe "User's customers" do
  it "contact does not exist, contactInfo object is not returned" do
    res = HTTParty.post(
        'http://localhost:3001/',
        {
            body: {"name":"MYNAME"}.to_json,
            headers: {
                'Content-Type': 'application/json'
            }
        }
    )
    expect((JSON.parse(res.body))['name']).to eq "MYNAME"
  end

  it "Without user name" do
    res = HTTParty.post(
        'http://localhost:3001/',
        {
            body: {}.to_json,
            headers: {
                'Content-Type': 'application/json'
            }
        }
    )
    expect((JSON.parse(res.body))['name']).to be nil

  end
end

=begin
Notes: - the contactInfo object is not returned
when the customer doesn't have contact information in our database;
 and - customer size is: Small, when # of employees is <= 10; Medium when it is <= 1000; Big otherwise.
=end